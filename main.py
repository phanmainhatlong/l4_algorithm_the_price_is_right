import sys
import tkinter as tk
from includes.constants import WIN_WIDTH, WIN_HEIGHT, LIGHT_BLUE, BLACK
from includes.game import Game
from includes.game_testmode import GameWithTestMode
from includes.scripts import test_data
from includes.config import Config

class WelcomeScreen:
    """WelcomeScreen:
    Class to display welcome screen for TPIR.
    -   Start: Start game
    -   Config: Config data.json
    Run main.py with --test enable test mode in game
    Run main.py with --verify run a data check before start game
    """
    def __init__(self, test_mode: bool | None = ..., verify: bool | None = ...):
        if test_mode and isinstance(test_mode, bool):
            self.test_mode = True
        else:
            self.test_mode = False
        if verify and isinstance(verify, bool):
            self.verify = True
        else:
            self.verify = False
        self.root = tk.Tk()
        self.root.title("The Price is Right: Title")
        self.root.geometry(f"{WIN_WIDTH}x{WIN_HEIGHT}+"
                           f"{int(self.root.winfo_screenwidth() / 2 - WIN_WIDTH / 2)}+"
                           f"{int(self.root.winfo_screenheight() / 2 - WIN_HEIGHT / 2)}")
        self.root.resizable(False, False)
        self.root.protocol("WM_DELETE_WINDOW", self.handle_close)
        self.root.config(bg=LIGHT_BLUE)

        self.config_gui = Config(self.root)
        self.config_gui.root.destroy()
        self.config_gui.closed = True


    def init_widget(self):
        """init_widget:
        Call this function initiate all widgets displayed on screen.
        """
        if self.test_mode:
            test_mode_label = tk.Label(self.root,
                                            text="Running in test mode", font=("times", 10),
                                            justify=tk.LEFT, bg=LIGHT_BLUE, fg=BLACK)
            test_mode_label.place(x=0, y=0, width=120, height=30)
        title_display = tk.Label(self.root, text="The Price\n\tis Right",
                                      font=("Times", 35), justify=tk.LEFT)
        title_display.place(x=100, y=100, width=400, height=150)
        start_button = tk.Button(self.root, text="Play", font=("Times", 20),
                                      command=self.game_start)
        start_button.place(x=200, y=350, width=200, height=50)
        config_button = tk.Button(self.root, text="Config", font=("Times", 20),
                                       command=self.data_config)
        config_button.place(x=200, y=450, width=200, height=50)


    def game_start(self):
        """game_start:
        Call this function start the game
        """
        if self.config_gui.closed:
            if self.test_mode:
                game = GameWithTestMode()
            else:
                game = Game()
            data = game.get_data_from_json()
            if self.verify and not test_data(data):
                print("Data test failed. Stop running.")
                sys.exit()
            else:
                game.init_widget()
                game.mainloop(self.root)


    def data_config(self):
        """data_config:
        Call this function open a text editor to edit data.json content
        """
        if self.config_gui.closed:
            self.config_gui.__init__(self.root)
            self.config_gui.get_data_from_json()
            self.config_gui.init_widget()


    def run(self):
        """run:
        Start Tkinter mainloop
        """
        self.root.mainloop()


    def handle_close(self):
        """handle_close:
        For tk protocol
        """
        self.root.destroy()


def main(test: bool | None = ..., verify: bool | None = ...):
    """Main function

    Args:
        test (bool | None, optional): test mode. Defaults to False
        verify (bool | None, optional): data verify. Defaults to False
    """
    if isinstance(test, bool):
        _test = test
    else:
        raise TypeError("test must be boolean type")
    if isinstance(verify, bool):
        _verify = verify
    else:
        raise TypeError("verify must be boolean type")
    gui = WelcomeScreen(_test, _verify)
    gui.init_widget()
    gui.run()


if __name__ == "__main__":
    TEST_ARG = False
    VERIFY_ARG = True
    if len(sys.argv) > 1:
        if "--test" in sys.argv:
            print("Test mode TRUE")
            TEST_ARG = True
        if "--no-verify" in sys.argv:
            print("Verify FALSE")
            VERIFY_ARG = False
    if not TEST_ARG:
        print("Test mode FALSE")
    if VERIFY_ARG:
        print("Verify TRUE")
    main(TEST_ARG, VERIFY_ARG)
