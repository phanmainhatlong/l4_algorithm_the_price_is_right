# Changelog


## (unreleased)

### New

* Feat: add proper guess counter. [phanmainhatlong@gmail.com]

* Feat: Add command line argument for verifying json data, display for max_guess. [phanmainhatlong@gmail.com]

### Fix

* Item image loading error. [phanmainhatlong@gmail.com]

  default image does not load when image is neither a valid file nor not exists in data.json

### Other

* Merge branch 'master' into 'main' [Phan Mai Nhật Long]

  fix: item image loading error

  See merge request phanmainhatlong/l4_algorithm_the_price_is_right!6

* Update CHANGELOG.md. [Phan Mai Nhật Long]

* Update CHANGELOG. [Phan Mai Nhật Long]

* Update CHANGELOG. [Phan Mai Nhật Long]

* Merge branch 'master' into 'main' [Phan Mai Nhật Long]

  other(fix, feat): fix exception handling, add test mode

  See merge request phanmainhatlong/l4_algorithm_the_price_is_right!5

* Other(fix, feat): fix some exception handling, add test mode. [phanmainhatlong@gmail.com]

  Fix some wrong exception handling, add missing exception handling. Test mode can be accessed by adding >--test when running main.py

* Add CHANGELOG. [Phan Mai Nhật Long]

* Merge branch 'master' into 'main' [Phan Mai Nhật Long]

  Remove not accessed auto-import libraries

  See merge request phanmainhatlong/l4_algorithm_the_price_is_right!4

* Remove not accessed auto-import libraries. [phanmainhatlong@gmail.com]

* Add LICENSE. [Phan Mai Nhật Long]

* Merge branch 'master' into 'main' [Phan Mai Nhật Long]

  Change title for main window and game window, edit .gitignore

  See merge request phanmainhatlong/l4_algorithm_the_price_is_right!3

* Change title for main window and game window, edit .gitignore. [phanmainhatlong@gmail.com]

* Update README.md. [Phan Mai Nhật Long]

* Merge branch 'master' into 'main' [Phan Mai Nhật Long]

  Update README

  See merge request phanmainhatlong/l4_algorithm_the_price_is_right!2

* Update README. [phanmainhatlong@gmail.com]

* Merge branch 'master' into 'main' [Phan Mai Nhật Long]

  Initial commit

  See merge request phanmainhatlong/l4_algorithm_the_price_is_right!1

* Initial commit. [phanmainhatlong@gmail.com]

* Initial commit. [Phan Mai Nhật Long]


