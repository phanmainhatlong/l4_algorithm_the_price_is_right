"""Entry

A module for Entry in pygame
"""

import re
from threading import Thread
from typing import Any, Callable, Literal
import pygame

ENTRY_ACTIVE = "#1C86EE"
ENTRY_INACTIVE = "#8DB6CD"
HEX_COLOR_PATTERN = "^#([0-9A-Fa-f]{6})$"

class Entry:
    """Entry
    Entry class for pygame
    """
    def __init__(self, master: pygame.Surface, font: pygame.font.Font,
                 text: str | None = "", active: Literal[True, False] = False,
                 expand_max: int | None = None, command: Callable[[], Any] | str = ...,
                 max_len: int | None = None, border_width: int = 1,
                 border_radius: int = -1, active_color: str = ENTRY_ACTIVE,
                 border_color: str = ENTRY_INACTIVE) -> None:
        """Entry

        Args:
            master (pygame.Surface): Surface to draw on
            font (pygame.font.Font): Font for text rendering
            text (str | None, optional): Init text.
            active (Literal[True, False], optional): Init state. Defaults to False.
            expand_max (int | None, optional): Max width expansion.
            command (Callable[[], Any] | str, optional):
                Command to call when RETURN key is pressed.
            max_len (int | None, optional): max input length.
            border_width (int | None, optional): border width.
            border_radius (int | None, optional): border roundness.
            active_color (str): border color when entry is active
            border_color (str): border color when entry is inactive
        """
        self._cleared_inputs = []
        self._recently_cleared = ""
        if master and isinstance(master, pygame.Surface):
            self.screen = master
        elif not isinstance(master, pygame.Surface):
            raise TypeError("master must be a Surface")
        if text and isinstance(text, str):
            self.text = f"{text}"
        else:
            self.text = ""
        if font and isinstance(font, pygame.font.Font):
            self.font = font
        elif not isinstance(font, pygame.font.Font):
            raise TypeError("font must be pygame.font.Font")
        if expand_max and isinstance(expand_max, int):
            self.expand_max = expand_max
        elif expand_max is None:
            self.expand_max = None
        else:
            raise TypeError(f"invalid type for expand_max: {type(expand_max)}. Expected: int")
        if callable(command):
            self.command = command
        else:
            self.command = lambda: None
        if isinstance(active, bool):
            self.active = active
        else:
            raise TypeError(f"invalid type for active: {type(active)}. Expected: bool")
        if isinstance(max_len, int):
            self.max_len = max_len
        elif max_len is None:
            self.max_len = None
        else:
            raise TypeError(f"invalid type for max_len: {type(max_len)}. Expected: int or None")
        if isinstance(border_width, int):
            self.border_width = border_width
        else:
            raise TypeError(f"invalid type for border_width: {type(border_width)}. Expected: int")
        if isinstance(border_radius, int):
            self.border_radius = border_radius
        else:
            raise TypeError(f"invalid type for border_radius: {type(border_radius)}. Expected: int")
        if isinstance(active_color, str):
            if (re.match(HEX_COLOR_PATTERN, active_color)):
                self.active_color = active_color
            else:
                raise ValueError(f"invalid value {active_color} for active_color")
        else:
            raise TypeError(f"invalid type for active_color: {type(active_color)}. Expected: str")
        if isinstance(border_color, str):
            if (re.match(HEX_COLOR_PATTERN, border_color)):
                self.border_color = border_color
            else:
                raise ValueError(f"invalid value {border_color} for border_color")
        else:
            raise TypeError(f"invalid type for border_color: {type(border_color)}. Expected: str")

    def config(self, text: str | None = ..., font: pygame.font.Font | None = ...,
               active: Literal[True, False] = ..., expand_max: int | None = ...,
               max_len: int | None = ..., border_width: int | None = ...,
               border_radius: int | None = ..., active_color: str = ...,
               border_color: str = ...) -> None:
        """Configure Entry attributes

        Args:
            text (str | None, optional): Text.
            font (pygame.font.Font | None, optional): Change font.
            active (Literal[True, False], optional): Change state.
            expand_max (int | None, optional): Change max expansion.
            max_len (int | None, optional): Change max length.
            border_width (int | None, optional): Change border width
            border_radius (int | None, optional): Change border roundness
            active_color (str): Change active color
            border_color (str): Change border color
        """
        if isinstance(text, str):
            self.text = f"{text}"
        if isinstance(font, pygame.font.Font):
            self.font = font
        if isinstance(active, bool):
            self.active = active
        if isinstance(expand_max, int):
            self.expand_max=expand_max
        if isinstance(max_len, int):
            self.max_len = max_len
        if isinstance(border_radius, int):
            self.border_radius = border_radius
        if isinstance(border_width, int):
            self.border_width = border_width
        if isinstance(active_color) and re.match(HEX_COLOR_PATTERN, active_color):
            self.active_color = active_color
        if isinstance(border_color) and re.match(HEX_COLOR_PATTERN, border_color):
            self.border_color = border_color


    def place(self, x: int, y: int, width: int, height: int, color: str ="#000000"):
        """Place widget on screen

        Args:
            x (int): Position on x-axis
            y (int): Position on y-axis
            width (int): Entry width
            height (int): Entry height
            color (str, optional): Text color. Defaults to "#000000".
        """
        self.entry_rect = pygame.Rect((x, y), (width, height))
        self.text_surface = self.font.render(self.text, True, color)
        if self.text_surface.get_width() > self.entry_rect.width - 5:
            if self.expand_max:
                self.entry_rect.width = min(self.text_surface.get_width() + 5, self.expand_max)
                if self.text_surface.get_width() > self.expand_max - 5:
                    self.text = self.text[:-1]
            else:
                self.text = self.text[:-1]
        if self.active:
            rect_color = self.active_color
        else:
            rect_color = self.border_color
        pygame.draw.rect(self.screen, rect_color, self.entry_rect, self.border_width, border_radius=self.border_radius)
        self.screen.blit(self.text_surface,
                         (x+5, y + int(height/2 - self.text_surface.get_height()/2)))

    def handle_entry_events(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.entry_rect.collidepoint(event.pos):
                self.active = True
            else:
                self.active = False
        if event.type == pygame.KEYDOWN:
            if self.active:
                if event.key == pygame.K_BACKSPACE:
                    self.text = self.text[:-1]
                elif event.key == pygame.K_RETURN:
                    if self.command:
                        Thread(target=self.command).start()
                    self._cleared_inputs.append(self.text)
                    self._recently_cleared = self.text
                    self.text = ""
                else:
                    if self.max_len:
                        if len(self.text) < self.max_len:
                            self.text += event.unicode
                    else:
                        self.text += event.unicode


    def get(self) -> str:
        """Get entry text

        Returns:
            str: Text from entry
        """
        return str(self.text)


if __name__ == "__main__":
    print("This is a module")
