"""Game.py

Module contain Game class
"""

# STANDARD LIBRARY
import shutil
import os
import json
import random
import math
from pathlib import Path
from typing import Any
from tkinter import messagebox as mb
from collections import deque
# PIP PACKAGE
from PIL import Image
import pygame
# USER PACKAGE
from includes.button import Button
from includes.entry import Entry
from includes.label import Label
from includes.sprites import Item
from includes.scripts import scale_image, pil_image_to_pygame_surface
from includes.constants import ITEM_DISPLAY_POS, DEFAULT_DATA, GAME_WIDTH, GAME_HEIGHT, FPS


class Game:
    """Game

    Game class
    """
    def __init__(self) -> None:
        """init
        Init Game class
        """
        pygame.init()
        pygame.display.set_caption("The Price is Right: ...")

        self.screen = pygame.display.set_mode((GAME_WIDTH, GAME_HEIGHT))
        self.clock = pygame.time.Clock()

        if not os.path.exists(rf"{Path(__file__).parent.parent}\assets"):
            shutil.copytree(rf"{os.getcwd()}\assets", rf"{Path(__file__).parent.parent}\assets")
        try:
            self.font = pygame.font.Font(
                rf"{Path(__file__).parent.parent}\assets\fonts\times.ttf", 20)
        except Exception:
            try:
                self.font = pygame.font.SysFont(pygame.font.get_default_font(), size=20)
            except Exception:
                self.running = False
        try:
            self.default_item_image = pil_image_to_pygame_surface(
                scale_image(Image.open(rf"{Path(__file__).parent.parent}\assets\default.png")))
        except Exception:
            self.default_item_image = pygame.Surface((200, 200))

        # Attributes
        self.running = True
        self.ongoing_action = False
        self.data = {}
        self.max_guess = {}
        self.score = {}
        self.buttons = []
        self.entries = []
        self.keys = []
        self.passed = []
        self.on_key = ""
        self.item_group = pygame.sprite.Group()
        self.max_guess_compute = lambda key: math.floor(
            math.log(abs(self.data[key]["price_max"] - self.data[key]["price_min"]), 2))


    def get_data_from_json(self):
        """Read data from json
        """
        if not os.path.exists(rf"{Path(__file__).parent.parent}\data.json"):
            self.create_new_json(DEFAULT_DATA)

        try:
            with open(rf"{Path(__file__).parent.parent}\data.json", "r", encoding="utf-8"):
                pass
        except Exception:
            self.create_new_json(DEFAULT_DATA)

        try:
            with open(rf"{Path(__file__).parent.parent}\data.json", "r", encoding="utf-8") as file:
                raw_data = file.read().encode("utf-8")
                data = json.loads(raw_data.decode())
        except Exception:
            print("JSON is corrupted")
            self.running = False
        else:
            self.data = data
            self.keys = self.get_data_keys()
            self.key_cycle = deque(self.keys)
            self.on_key = self.key_cycle[0]
            self.on_item = 1
            pygame.display.set_caption(f"The Price is Right: Item 1/{len(self.keys)}")
            return data


    def create_new_json(self, data: dict):
        """Create new json

        Args:
            data (dict): init data
        """
        try:
            with open(rf"{Path(__file__).parent.parent}\data.json", "w", encoding="utf-8") as file:
                json_string = json.dumps(data, indent=4, ensure_ascii=False).encode("utf-8")
                file.write(json_string.decode())
        except OSError:
            self.running = False


    def get_data_keys(self) -> list:
        """Get keys from self.data

        Returns:
            list: a list of keys
        """
        return list(self.data.keys())


    def get_image(self) -> Any:
        """Get image from data

        Returns:
            Any: image
        """
        try:
            pil_image = Image.open(self.data[self.on_key]["image"])
        except FileNotFoundError:
            image = self.default_item_image
        except IOError:
            image = self.default_item_image
        except KeyError:
            image = self.default_item_image
        except AttributeError:
            image = self.default_item_image
        else:
            image = pil_image_to_pygame_surface(scale_image(pil_image))
        return image


    def get_price(self) -> int:
        """Get price from data, or generate new price if not exists

        Returns:
            int: price
        """
        try:
            price = self.data[self.on_key]["price"]
        except KeyError:
            price = random.randint(self.data[self.on_key]["price_min"],
                                   self.data[self.on_key]["price_max"])
        return price


    def init_widget(self):
        """Init widgets on screen
        """
        image = self.get_image()
        price = self.get_price()

        self.item_display = Item(pos=ITEM_DISPLAY_POS, image=image, price=price)

        self.max_guess[self.on_key] = self.max_guess_compute(self.on_key)

        if self.max_guess[self.on_key] <= 0:
            self.max_guess[self.on_key] = 3

        self.item_group.add(self.item_display)

        self.max_guess_label = Label(master=self.screen,
                                     font=self.font,
                                     text=f"Max guess: {self.max_guess[self.on_key]}")
        self.next_button = Button(master=self.screen,
                                  text="Next", font=self.font, command=self.next_item)
        self.previous_button = Button(master=self.screen,
                                      text="Previous", font=self.font, command=self.previous_item)
        self.range_label = Label(
            master=self.screen,
            font=self.font, text=f"{self.data[self.on_key]['price_min']}    ~    "
                                 f"{self.data[self.on_key]['price_max']}")
        self.name_label = Label(master=self.screen,
                                font=self.font, text=f"{self.data[self.on_key]['name']}")
        self.price_entry = Entry(master=self.screen, font=self.font, command=self.submit_price)
        self.buttons = [self.next_button, self.previous_button]
        self.entries = [self.price_entry]


    def draw_widget(self):
        """Draw widget on screen
        """
        self.max_guess_label.place(0, 0, 200, 50)
        self.name_label.place(150, 100, 400, 50)
        self.range_label.place(150, 450, 400, 50)
        self.next_button.place(550, 275, 100, 50)
        self.previous_button.place(50, 275, 100, 50)
        self.price_entry.place(150, 525, 400, 50)


    def next_item(self):
        """Change to next item.
        When on last time, change to first item
        """
        self.key_cycle.rotate(-1)
        self.on_key = self.key_cycle[0]
        self.keys = list(self.key_cycle)

        if self.on_item + 1 > len(self.keys):
            self.on_item = 1
        else:
            self.on_item += 1

        image = self.get_image()
        price = self.get_price()

        try:
            if not self.max_guess[self.on_key]:
                pass
        except KeyError:
            self.max_guess[self.on_key] = self.max_guess_compute(self.on_key)
            if self.max_guess[self.on_key] <= 0:
                self.max_guess[self.on_key] = 3

        self.item_display.config(image=image, price=price)
        self.range_label.config(text=f"{self.data[self.on_key]['price_min']}"
                                "    ~    "
                                f"{self.data[self.on_key]['price_max']}")
        self.name_label.config(text=f"{self.data[self.on_key]['name']}")
        self.max_guess_label.config(text=f"Max guess: {self.max_guess[self.on_key]}")


    def previous_item(self):
        """Change to previous item
        When on first item, change to last item
        """
        self.key_cycle.rotate(1)
        self.on_key = self.key_cycle[0]
        self.keys = list(self.key_cycle)

        if self.on_item - 1 <= 0:
            self.on_item = len(self.keys)
        else:
            self.on_item -= 1

        image = self.get_image()
        price = self.get_price()
        try:
            if not self.max_guess[self.on_key]:
                pass
        except:
            self.max_guess[self.on_key] = self.max_guess_compute(self.on_key)
            if self.max_guess[self.on_key] <= 0:
                self.max_guess[self.on_key] = 3
        self.item_display.config(image=image, price=price)
        self.range_label.config(text=f"{self.data[self.on_key]['price_min']}"
                                "    ~    "
                                f"{self.data[self.on_key]['price_max']}")
        self.name_label.config(text=f"{self.data[self.on_key]['name']}")
        self.max_guess_label.config(text=f"Max guess: {self.max_guess[self.on_key]}")


    def discard_item_next(self, key: str):
        """Change to next item, discard item[key]

        Args:
            key (str): key to remove
        """
        try:
            self.data.pop(key)
        except KeyError:
            print("Key error in discard_item_next")
        else:
            self.key_cycle.rotate(-1)
            self.on_key = self.key_cycle[0]
            self.keys = list(self.key_cycle)
            self.keys.remove(key)
            self.key_cycle = deque(self.keys)

            if self.on_item + 1 > len(self.keys):
                self.on_item = len(self.keys)

            if self.keys:
                image = self.get_image()
                price = self.get_price()
                try:
                    if not self.max_guess[self.on_key]:
                        pass
                except:
                    self.max_guess[self.on_key] = self.max_guess_compute(self.on_key)
                    if self.max_guess[self.on_key] <= 0:
                        self.max_guess[self.on_key] = 3

                self.item_display.config(image=image, price=price)
                self.range_label.config(text=f"{self.data[self.on_key]['price_min']}"
                                        "    ~    "
                                        f"{self.data[self.on_key]['price_max']}")
                self.name_label.config(text=f"{self.data[self.on_key]['name']}")
                self.max_guess_label.config(text=f"Max guess: {self.max_guess[self.on_key]}")
            else:
                self.running = False
                mb.showinfo("Info", "You win")


    def submit_price(self):
        """Handle price submit (Entry)
        """
        try:
            textvariable = int(self.price_entry.text)
        except ValueError:
            mb.showerror("Error", "Price must be an integer")
            return

        if (textvariable < self.data[self.on_key]["price_min"] or
            textvariable > self.data[self.on_key]["price_max"]):
            mb.showwarning("Warning", "Input out of range")
            return

        if textvariable > self.item_display.price:
            mb.showinfo("Info", "Your price is higher than item price")
        elif textvariable < self.item_display.price:
            mb.showinfo("Info", "Your price is lower than item price")
        else:
            mb.showinfo("Info", "Correct")
            self.discard_item_next(self.on_key)
            return

        if self.max_guess[self.on_key] - 1 >= 0:
            self.max_guess[self.on_key] -= 1
            self.max_guess_label.config(text=f"Max guess: {self.max_guess[self.on_key]}")
        else:
            if self.test_mode:
                print("game:Game:submit_price: In test mode. Resetting max_guess.")
                mb.showinfo("Info", "In test mode, resetting max_guess")
                self.max_guess[self.on_key] = self.max_guess_compute(self.on_key)
                self.max_guess_label.config(text=f"Max guess: {self.max_guess[self.on_key]}")
                return
            self.running = False
            mb.showinfo("Info", "Game over")


    def mainloop(self, tk_root):
        """Pygame's mainloop

        Args:
            tk_root (Tkinter's root): WelcomeScreen root.
        """
        tk_root.destroy()
        while self.running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    return
                for entry in self.entries:
                    entry.handle_entry_events(event)

            self.screen.fill("#FFFFFF")
            self.draw_widget()
            self.item_group.draw(self.screen)
            self.item_group.update()
            pygame.display.set_caption(f"The Price is Right: Item {self.on_item}/{len(self.keys)}")
            pygame.display.flip()
            self.clock.tick(FPS)


if __name__ == "__main__":
    print("Can not start as main")
