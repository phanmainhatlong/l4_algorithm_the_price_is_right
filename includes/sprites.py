from abc import abstractproperty
import pygame

class Item(pygame.sprite.Sprite):
    """Item Sprite

    Args:
        pos (tuple): Position in format (x, y)
        image (pygame.Surface): A pygame surface as display image
        default_alpha (int | None, optional): Default alpha for image
        price (int | None, optional): Price of item
    """
    def __init__(self, pos: tuple, image: pygame.Surface,
                 default_alpha: int | None = ...,
                 price: int | None = ..., *groups: abstractproperty) -> None:
        super().__init__(*groups)
        if image and isinstance(image, pygame.Surface):
            self.image = image
        elif image and not isinstance(image, pygame.Surface):
            raise WrongArgumentTypeProvided(("pygame.Surface"), "image")
        else:
            raise MissingRequiredArgument("image")
        if pos and isinstance(pos, tuple):
            self.pos = pos
        elif not isinstance(pos, tuple):
            raise WrongArgumentTypeProvided(("tuple"), "pos")
        if default_alpha and isinstance(default_alpha, int):
            self.default_alpha = default_alpha
        else:
            self.default_alpha = 255
        if price and isinstance(price, int):
            self.price = price
        else:
            self.price = 0
        self.init_sprite()


    def init_sprite(self):
        """Init sprite
        """
        self.rect = self.image.get_rect()
        self.rect.topleft = (self.pos[0], self.pos[1])
        self.alpha = self.default_alpha


    def config(self, image: pygame.Surface | None = ...,
               pos: tuple | None = ..., price: int | None = ...):
        """Configure sprite

        Args:
            image (pygame.Surface | None, optional): Change to new image
            pos (tuple | None, optional): Chanage to new position
            price (int | float | None, optional): Change to new price
        """
        if image and isinstance(image, pygame.Surface):
            self.image = image
        if pos and isinstance(pos, tuple):
            self.pos = pos
        if price and isinstance(price, int):
            self.price = price


    def update(self) -> None:
        """Update sprite
        """


if __name__ == "__main__":
    print("This is a module")
else:
    from includes.exceptions import MissingRequiredArgument, WrongArgumentTypeProvided
        