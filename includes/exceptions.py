class MissingRequiredArgument(BaseException):
    """Exception raised when function missing a required argument.

    Args:
        missing_arg: Argument that are missing
    """
    def __init__(self, missing_arg: str | None = ...) -> None:
        self.missing_arg = missing_arg
        super().__init__(self.missing_arg)


class WrongArgumentTypeProvided(BaseException):
    """Exception raised when type of argument provided is wrong

    Args:
        accepted_types: Accepted types
    """
    def __init__(self, accepted_types: list | tuple | set = ...,
                 for_what: str | None = ...) -> None:
        self.accepted_types = accepted_types
        super().__init__(f"Accepted types for {for_what}: {self.accepted_types}")


class PassingWrongValue(BaseException):
    """Exception raised when passing wrong value to argument

    Args:
        accepted_values: accepted values
    """
    def __init__(self, accepted_values: list | tuple | set | None = ...,
                 for_what: str | None = ...) -> None:
        self.accepted_values = accepted_values
        super().__init__(f"Accepted values for {for_what}: {self.accepted_values}")
