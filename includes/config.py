import tkinter as tk
import json
import os
from tkinter import Misc, messagebox as mb
from pathlib import Path

class Config:
    """Config:

    Args:
        master (Misc | None, optional): Master for this TopLevel widget.

    Raises:
        MissingRequiredArgument: Missing master
    """
    def __init__(self, master: Misc | None = ...) -> None:
        if master and master != ...:
            self.master = master
        else:
            raise MissingRequiredArgument("master")
        self.root = tk.Toplevel(master)
        self.root.title("The Price is Right: Config")
        self.root.geometry(f"{WIN_WIDTH}x{WIN_HEIGHT}+"
                           f"{int(self.root.winfo_screenwidth() / 2 - WIN_WIDTH / 2)}+"
                           f"{int(self.root.winfo_screenheight() / 2 - WIN_HEIGHT / 2)}")
        self.root.resizable(False, False)
        self.root.wm_transient(master)
        self.root.wm_protocol("WM_DELETE_WINDOW", self.handle_close)

        self.closed = False
        self.data = {}

        # Pre-define
        self.menubar = None
        self.filemenu = None
        self.text_scrollx = None
        self.text_scrolly = None
        self.textarea = None


    def get_data_from_json(self):
        """Method to get data from data.json
        """
        if not os.path.exists(rf"{Path(__file__).parent.parent}\data.json"):
            print(f"config:Config:get_data_from_json: file "
                  f"\"{Path(__file__).parent.parent}\\data.json\""
                  "does not exists. Creating a new one.")
            print("config:Config:get_data_from_json: Using default data to write.")
            self.create_new_json(DEFAULT_DATA)

        try:
            with open(rf"{Path(__file__).parent.parent}\data.json", "r", encoding="utf-8"):
                pass
        except OSError as error:
            print(f"Exception in config:Config:get_data_from_json:\n{error}")
            print("Re-creating new data.json\n")
            print("Using default data to write new file")
            self.create_new_json(DEFAULT_DATA)

        try:
            with open(rf"{Path(__file__).parent.parent}\data.json", "r", encoding="utf-8") as file:
                raw_data = file.read().encode("utf-8")
                data = json.loads(raw_data.decode())
        except OSError as error:
            print("Exception in config:Config:get_data_from_json: While reading json file, "
                  f"this exception occurred:\n{error}")
            print("Do you want to re-create json file using default data?")
            prompt = input("Recreate prompt ([Y]es/[n]o) ").lower()
            match prompt:
                case ("yes" | "y"):
                    print("config:Config:get_data_from_json: User accept to re-create json.")
                    print("config:Config:get_data_from_json: Using default data to write")
                    self.create_new_json(DEFAULT_DATA)
                case ("no" | "n"):
                    print("config:Config:get_data_from_json: User refused to re-create json.")
                    self.handle_close()
        else:
            self.data = data
            print("config:Config:get_data_from_json: Read data from json successfully.")


    def create_new_json(self, data: dict):
        """_summary_

        Args:
            data (dict | str): initial data for data.json
        """
        with open(rf"{Path(__file__).parent.parent}\data.json", "w", encoding="utf-8") as file:
            json_string = json.dumps(data, indent=4, ensure_ascii=False).encode("utf-8")
            file.write(json_string.decode())


    def init_widget(self):
        """Init widgets on screen
        """
        # Init
        self.menubar = tk.Menu(self.root)
        self.filemenu = tk.Menu(self.menubar, tearoff=0)
        self.textarea = tk.Text(self.root, font=("monospace", 16), wrap=tk.NONE)
        self.text_scrollx = tk.Scrollbar(self.root, command=self.textarea.xview,
                                         orient=tk.HORIZONTAL)
        self.text_scrolly = tk.Scrollbar(self.root, command=self.textarea.yview)

        # Config
        self.textarea.config(xscrollcommand=self.text_scrollx.set,
                             yscrollcommand=self.text_scrolly.set)
        self.textarea.insert(tk.END, json.dumps(self.data, indent=4,
                                                ensure_ascii=False).encode("utf-8").decode())
        self.filemenu.add_command(label="Save", command=self.save_settings_to_file)
        self.filemenu.add_command(label="Discard", command=self.discard_changes)
        self.filemenu.add_command(label="Quit", command=self.quit)
        self.menubar.add_cascade(label="File", menu=self.filemenu)
        self.root.config(menu=self.menubar)
        # Place
        self.textarea.place(x=20, y=40, width=560, height=400)
        self.text_scrollx.place(x=20, y=440, width=560, height=20)
        self.text_scrolly.place(x=580, y=40, width=20, height=400)


    def save_settings_to_file(self):
        """Save changes to data.json
        """
        with open(rf"{Path(__file__).parent.parent}\data.json", "w", encoding="utf-8") as file:
            json_string = self.textarea.get("1.0", tk.END).encode("utf-8").decode()
            json.dump(json.loads(json_string), file, indent=4, ensure_ascii=False)
        self.init_widget()


    def discard_changes(self):
        """Discard changes made in data.json
        """
        print(json.dumps(self.data, indent=4))
        self.textarea.delete("0.0", tk.END)
        self.textarea.insert(tk.END, json.dumps(self.data, indent=4))


    def quit(self):
        """Quit config
        """
        prompt = mb.askyesno("Save & Quit", "Do you want to save your changes before quit?")
        if prompt:
            self.save_settings_to_file()
            self.handle_close()
        else:
            self.discard_changes()
            self.handle_close()


    def handle_close(self):
        """root protocol
        """
        self.closed = True
        self.root.destroy()


if __name__ == "__main__":
    from constants import WIN_WIDTH, WIN_HEIGHT, DEFAULT_DATA
    from exceptions import MissingRequiredArgument
else:
    from includes.constants import WIN_WIDTH, WIN_HEIGHT, DEFAULT_DATA
    from includes.exceptions import MissingRequiredArgument
