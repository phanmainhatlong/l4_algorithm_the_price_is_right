"""Button

A module for button in pygame
"""

from typing import Any, Callable, Literal
from threading import Thread
import pygame


class Button:
    """Button:

    Args:
        master (pygame.Surface): A surface to draw on
        font (pygame.font.Font): Pygame font, for rendering text.
        text (str | None, optional): Text to render on Button. Optional.
        command (Callable[[], Any] | str, optional):
            Function to be called when button is pressed. Optional
        state (Literal["normal", "disabled"], optional):
            Default state for button. Defaults to "normal"
        border_radius (int | None, optional):
            Increase this number make button edges rounder. Defaults to -1
    """
    def __init__(self, master: pygame.Surface, font: pygame.font.Font, text: str | None = ...,
                 command: Callable[[], Any] | str = ...,
                 state: Literal["normal", "disabled"] = ..., border_radius: int | None = ...):
        if master and isinstance(master, pygame.Surface):
            self.screen = master
        elif not isinstance(master, pygame.Surface):
            raise TypeError("master must be a Surface")
        if font and isinstance(font, pygame.font.Font):
            self.font = font
        elif not isinstance(font, pygame.font.Font):
            raise TypeError("font must be pygame.font.Font")
        if text and isinstance(text, str):
            self.text = text
        else:
            self.text = ""
        if command and callable(command):
            self.command = command
        else:
            self.command = lambda: None
        if state and isinstance(state, str):
            self.state = state.lower()
        else:
            self.state = "normal"
        if border_radius and isinstance(border_radius, int):
            self.border_radius = border_radius
        else:
            self.border_radius = -1
        if self.state == NORMAL:
            self.button_color = NORMAL_STATE
        else:
            self.button_color = DISABLED_STATE
        self.pressed = False
        self.function_executed = False


    def place(self, x: int, y: int, width: int, height: int):
        """Place button on Surface

        Args:
            x (int): Position on x-axis
            y (int): Position on y-axis
            width (int): Button's width
            height (int): Button's height
        """
        self.top_rect = pygame.Rect((x, y), (width, height))
        self.text_surface = self.font.render(self.text, True, WHITE)
        self.text_rect = self.text_surface.get_rect(
            center=self.top_rect.center)
        self.text_rect.center = self.top_rect.center

        pygame.draw.rect(self.screen, self.button_color, self.top_rect,
                         border_radius=self.border_radius)
        self.screen.blit(self.text_surface, self.text_rect)
        self.check_click()


    def config(self, text: str | None = ..., font: pygame.font.Font | None = ...,
               command: Callable[[], Any] | str = ..., border_radius: int | None = ...,
               state: Literal["normal", "disabled"] = ...):
        """Configure button attributes

        Args:
            text (str | None, optional): Change button text. Optional
            font (pygame.font.Font | None, optional): Change button font. Optional
            command (Callable[[], Any] | str, optional): Change command called. Optional
            border_radius (int | None, optional): Change border radius. Optional
            state (Literal["normal", "disabled"]): Change button state. Optional

        Raises:
            PassingWrongValue: Passing wrong value to state
        """
        if text and isinstance(text, str):
            self.text = text
        if font and isinstance(font, pygame.font.Font):
            self.font = font
        if command and callable(command):
            self.command = command
        if state and isinstance(state, str):
            if state not in ("disabled", "normal"):
                raise ValueError("Only \"disabled\" or \"normal\" allowed.")
            self.state = state.lower()
            self.button_color = NORMAL_STATE if self.state == "normal" else DISABLED_STATE
        if border_radius and isinstance(border_radius, int):
            self.border_radius = border_radius


    def check_click(self):
        """Check button click
        """
        if self.state == NORMAL:
            mouse_pos = pygame.mouse.get_pos()
            if self.top_rect.collidepoint(mouse_pos):
                if pygame.mouse.get_pressed()[0]:
                    self.button_color = DOWN_STATE
                    self.pressed = True
                    if not self.function_executed:
                        Thread(target=self.command).start()
                        self.function_executed = True
                else:
                    if self.pressed:
                        self.pressed = False
                        self.function_executed = False
                        self.button_color = NORMAL_STATE


if __name__ == "__main__":
    print("This is a module")
else:
    from includes.constants import WHITE, NORMAL, NORMAL_STATE, DISABLED_STATE, DOWN_STATE
