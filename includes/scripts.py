import os
import contextlib
from typing import Any
import pygame
from PIL import Image

#!: From https://stackoverflow.com/a/44231784
def scale_image(image: Any, min_size: int = 200, fill_color = (0, 0, 0)) -> Image:
    """scale_image
    Scale image to min_size

    Args:
        image (Any): _description_
        min_size (int): min size
        fill_color (tuple, optional): color, in rgb

    Returns:
        _type_: _description_
    """
    size = min_size
    image = image.resize((190,190))
    new_im = Image.new('RGB', (size, size), fill_color)
    new_im.paste(image, (5, 5))
    return new_im


#!: From https://stackoverflow.com/a/64182629
def pil_image_to_pygame_surface(pil_image: Image):
    """Convert PIL Image to pygame Surface

    Args:
        pil_image (Image): pil image

    Returns:
        Pygame surface
    """
    return pygame.image.fromstring(pil_image.tobytes(),
                                   pil_image.size, pil_image.mode).convert_alpha()


def test_data(data: dict, required_keys: list | tuple = ("price_min", "price_max", "name")) -> bool:
    """Test data

    Args:
        required_keys (list | tuple, optional): a list of required keys

    Returns:
        bool: data test result
    """
    keys = list(data.keys())
    devnull = open(os.devnull, "w", encoding="utf-8")
    for key in keys:
        for rkey in required_keys:
            try:
                with contextlib.redirect_stdout(devnull):
                    print(data[key][rkey])
            except KeyError:
                print(f"Key \"{rkey}\" is missing in item \"{key}\"")
                devnull.close()
                return False
            else:
                print(f"Key \"{rkey}\" found in item \"{key}\"")
                if rkey == "price_min":
                    p_min = data[key][rkey]
                    if p_min < 0:
                        print(f"\"price_min\" ({data[key][rkey]}) "
                            f"of \"{key}\" can not be negative")
                        devnull.close()
                        return False
                if rkey == "price_max":
                    p_max = data[key][rkey]
                    if p_max <= 0:
                        print(f"\"price_max\" ({data[key][rkey]}) of \"{key}\""
                            " can not lower or equal to zero (0)")
                        devnull.close()
                        return False
        if p_min > p_max:
            print(f"\"price_min\" ({p_min}) of \"{key}\" larger than "
                    f"\"price_max\" ({p_max}) of \"{key}\"")
            devnull.close()
            return False
    devnull.close()
    return True
