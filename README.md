# l4_algorithm_the_price_is_right

## Description
For SteamForVietnam's CS101 Lesson 4.
Inspired by "The Price is Right" TV Show

## Usage
- On first run:
> pip3 install -r requirements.txt
- Run:
> python3 main.py

to start

## Project status
- [x] Main
- [x] Button
- [x] Entry
- [x] Label
- [x] Game
- [ ] Config
## Detailed Status
- [x] Main
    - [x] UI
        - [x] Title Label
        - [x] Buttons
            - [x] Start Button
            - [x] Config Button
    - [x] Functionality
        - [x] Start Game
        - [x] Start Config
        - [x] Exit
    - [ ] Decoration
        - [ ] Background image
        - [ ] Text Font
        - [ ] Button display
- [x] Game
    - [x] UI
        - [x] Item image
        - [x] Buttons
            - [x] Next Button
            - [x] Previous Button
            - ~~[ ] Submit price Button~~ Use RETURN key instead
        - [x] Price range display Label
        - [x] Price Entry
    - [x] Functionality
        - [x] Get data from JSON
        - [x] Display image from data
        - [x] Displaying UI components
        - [x] Next item
        - [x] Previous item
        - [x] Check price (and move to next item if correct)
    - [ ] Others
        - [ ] ~~Animations~~
        - [ ] Background
        - [ ] SFX & Music
- [ ] Config
    - [ ] UI
        - [x] Display data.json content
        - [x] Scroll bars
        - [ ] Entry
        - [ ] Button
        - [x] Menu
            - [x] Save
            - [x] Discard
            - [x] Quit
    - [ ] Functionality
        - [x] Load data from file
        - [x] Edit data
        - [x] Save data to file
        - [x] Exit
        - [ ] Handle commands from entry
    - [x] Others
        - None

