@echo off
REM target -> one directory containing all required libraries and an executable
REM Running this will be faster
set curdir=%cd%
set wp=".\build\od"
set dp="..\dist\tpir_od"
set on="LS4_The_Price_is_Right"
set cp="%dp:"=%\%on:"=%\assets"
cd %~dp0\
pip install -r "..\requirements.txt"
pip install pyinstaller
pyinstaller "..\main.py" -n %on% --workpath %wp% --specpath %wp% --distpath %dp% --noconsole --noconfirm
mkdir %cp%
xcopy "..\assets" %cp% /E /H /C /I
rmdir /s /q %wp%
cd %curdir%
@echo on