@echo off
REM target -> one directory containing all required libraries and an executable
REM Running this will be faster
set curdir=%cd%
set wp=".\build\odc"
set dp="..\dist\tpir_odc"
set on="LS4_The_Price_is_Right"
set cp="%dp:"=%\%on:"=%\assets"
cd %~dp0\
pip install -r "..\requirements.txt"
pip install pyinstaller
pyinstaller "..\main.py" -n LS4_The_Price_is_Right --workpath %wp% --specpath %wp% --distpath %dp% --noconfirm
mkdir %cp%
xcopy "..\assets" %cp% /E /H /C /I
rmdir /s /q %wp%
cd %curdir%
@echo on