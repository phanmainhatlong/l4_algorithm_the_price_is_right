@echo off
REM target -> one executable with no console window
REM Running this is quite slow because it must unpack all packed libraries to %temp% before actually running
set curdir=%cd%
set wp=".\build\of"
set dp="..\dist\tpir_of"
set on="LS4_The_Price_is_Right"
set cp="%dp:"=%\assets"
cd %~dp0\
pip install -r "..\requirements.txt"
pip install pyinstaller
pyinstaller "..\main.py" -n LS4_The_Price_is_Right --workpath %wp% --specpath %wp% --distpath %dp% --noconsole --onefile --noconfirm
mkdir %cp%
xcopy "..\assets" %cp% /E /H /C /I
rmdir /s /q %wp%
cd %curdir%
@echo on